# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 1.1.0 (2021-06-24)


### Features

* add standard release support! ([a1a8518](https://gitlab.com/skrishnan2001/auto-changelog/commit/a1a8518d530559967988ac151b8143f410916821))


### Bug Fixes

* create nodejs application ([7a4d5d5](https://gitlab.com/skrishnan2001/auto-changelog/commit/7a4d5d577420980dad9ff9ad56c614990c308bd1))
